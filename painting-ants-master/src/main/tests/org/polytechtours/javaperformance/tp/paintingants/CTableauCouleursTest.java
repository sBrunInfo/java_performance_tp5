package org.polytechtours.javaperformance.tp.paintingants;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for the class CTableauCouleurs.
 */
class CTableauCouleursTest {
	/**
	 * Tests if every single possible getCouleur() valid combination returns the right color.
	 */
	@org.junit.jupiter.api.Test
	void testGetCouleur() {
		for (int red = 0; red < CTableauCouleurs.NB_VAL ; red++) {
			for (int green = 0; green < CTableauCouleurs.NB_VAL ; green++) {
				for (int blue = 0; blue < CTableauCouleurs.NB_VAL ; blue++) {
					assertEquals(CTableauCouleurs.getCouleur(red, green, blue), new Color(red, green, blue));
					int rgb = red;
					rgb = (rgb << 8) + green;
					rgb = (rgb << 8) + blue;
					assertEquals(CTableauCouleurs.getCouleur(rgb), new Color(rgb));
				}
			}
		}
	}
}