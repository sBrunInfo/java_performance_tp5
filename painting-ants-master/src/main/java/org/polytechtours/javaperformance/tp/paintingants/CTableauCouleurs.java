package org.polytechtours.javaperformance.tp.paintingants;

import java.awt.*;

/**
 * Représente un tableau 3D statique contant une référence pour chaque couleur existante.
 * <p>
 * À utiliser au lieu de faire trop de <code>new Color()</code>...
 */
public class CTableauCouleurs {
	/**
	 * Valeurs d'une composante (R, G ou B) : 0 à 255
	 */
	public static final short NB_VAL = 256;
	/**
	 * Nombre de couleurs possibles au total
	 */
	public static final int NB_COUL = (int) Math.pow(NB_VAL, 3);
	/**
	 * Les références encapsulées dans un tableau 1D
	 */
	private static Color[] tabCouleurs;

	/*
	 * Initialisation statique du tableau
	 */
	static {
		tabCouleurs = new Color[NB_COUL];
		int r = 0, g = 0, b = 0;
		for (int i = 0; i < NB_COUL; i++) {
			tabCouleurs[i] = new Color(r, g, b);
			r++;
			if (r >= NB_VAL) {
				r = 0;
				g++;
			}
			if (g >= NB_VAL) {
				g = 0;
				b++;
			}
			if (b >= NB_VAL) {
				b = 0;
			}
		}
	}

	/**
	 * Décompose <code>rgb</code> en 3 entiers (rouge, vert, bleu) puis retourne <code>getCouleur(rouge, vert, bleu)</code>
	 *
	 * @param rgb : avec le rouge dans les bits 16-23, le vert dans les bits 8-15, et le bleu dans les bits 0-7
	 * @return une référence à un objet <code>Color</code> correspondant
	 */
	public static Color getCouleur(int rgb) {
		int red = (rgb >> 16) & 0xFF;
		int green = (rgb >> 8) & 0xFF;
		int blue = rgb & 0xFF;
		return getCouleur(red, green, blue);
	}

	/**
	 * @param r rouge
	 * @param g vert
	 * @param b bleu
	 * @return une référence à un objet <code>Color</code> correspondant
	 * @throws IllegalArgumentException si le moindre paramètre n'est pas entre 0 (inclus) et <code>NB_VAL</code> (non-inclus)
	 */
	public static Color getCouleur(int r, int g, int b) {
		if (r < 0 || r >= NB_VAL)
			throw new IllegalArgumentException("r doit être entre " + 0 + " (inclus) et " + NB_VAL + " (non inclus).");
		if (g < 0 || g >= NB_VAL)
			throw new IllegalArgumentException("g doit être entre " + 0 + " (inclus) et " + NB_VAL + " (non inclus).");
		if (b < 0 || b >= NB_VAL)
			throw new IllegalArgumentException("b doit être entre " + 0 + " (inclus) et " + NB_VAL + " (non inclus).");

		return tabCouleurs[r + (g * NB_VAL) + (b * NB_VAL * NB_VAL)];
	}
}
