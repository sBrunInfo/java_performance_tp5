package org.polytechtours.javaperformance.tp.paintingants;

/**
 * Représente un runnable qui effectue les déplacement d'une fourmi donnée.
 * <p>
 * Cela permet de définir le nombre de threads parallèles qui vont faire se déplacer les fourmis
 * avec un système de rotation
 */
public class CFourmiDeplaceur implements Runnable{

    // La fourmi déplacée
    private CFourmi fourmi;

    // La colonie dont provient la fourmi
    private CColonie colonie;

    // L'appli sur laquelle dessiner
    private PaintingAnts mApplis;

    // Un booléen pour vérifier si l'application est terminée
    private Boolean mContinue = Boolean.TRUE;
    private int num;

    /**
     * Fonction run au lancement du thread,
     * Gère le déplacement d'une fourmi et le passage à une autre fourmi
     */
    @Override
    public void run() {
        while (mContinue)
        {
            if (!mApplis.getPause()) {
                // On déplace la fourmi
                fourmi.deplacer();
                mApplis.compteur();
                num++;
                // Si on arrive à la fin de la colonie, on repart au début
                if(num >= colonie.getmColonie().size())
                    num = 0;
                // On passe à la fourmi suivante dans la file
                fourmi = colonie.getmColonie().get(num);
            }
        }
    }

    /**
     * Constructeur qui prend en paramètre la colonie et la position dans la file
     * Cela permettra de parcourir la file pour changer de fourmi à déplacer
     * @param colonie
     * @param num
     */
    public CFourmiDeplaceur(CColonie colonie, int num)
    {
        this.colonie = colonie;
        this.fourmi = colonie.getmColonie().get(num);
        this.num = num;
        this.mApplis = fourmi.getMAppli();
    }
}
